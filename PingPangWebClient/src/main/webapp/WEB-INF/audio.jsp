﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <div>
        <audio controls autoplay></audio>
        <button type="button" class="layui-btn layui-btn-sm layui-btn-normal" id="btnStart" onclick="startRecording()"><i class="layui-icon">&#xe6fc;</i>录制</button>
        <button type="button" class="layui-btn layui-btn-sm layui-btn-disabled"  id="btnStop" onclick="stopRecording()" > <i class="layui-icon">&#xe651;</i>停止</button>
        <button type="button" class="layui-btn layui-btn-sm layui-btn-disabled" id="btnPlay" onclick="playRecording()" disabled="disabled"><i class="layui-icon">&#xe652;</i>播放</button>
        <button type="button" class="layui-btn layui-btn-sm layui-btn-disabled" id="btnUpload" onclick="uploadAudio()" disabled="disabled"><i class="layui-icon">&#xe681;</i>上传音频</button>
    </div>

    <script type="text/javascript" src="${httpServletRequest.getContextPath()}/audio/HZRecorder.js"></script>
    <script src="${httpServletRequest.getContextPath()}/layui.js"></script>
    <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/css/layui.css"  media="all">
    <script>

        var recorder;

        var audio = document.querySelector('audio');

        //录音
        function startRecording() {
            console.log('开始录音')
            document.getElementById('btnStop').className = 'layui-btn layui-btn-sm';
            document.getElementById('btnStop').disabled = "";
            document.getElementById('btnUpload').className = 'layui-btn layui-btn-sm';
            document.getElementById('btnUpload').disabled = "";
            document.getElementById('btnPlay').className = 'layui-btn layui-btn-sm';
            document.getElementById('btnPlay').disabled = "";
            document.getElementById('btnStart').disabled = 'disabled';
            document.getElementById('btnStart').className = 'layui-btn layui-btn-sm layui-btn-disabled';
            HZRecorder.get(function (rec) {
                recorder = rec;
                recorder.start();
            }, {
                    sampleBits: 16,
                    sampleRate: 16000
                });
        }
         //停止录音
        function stopRecording() {
            recorder.stop();
            document.getElementById('btnStart').disabled = '';
            document.getElementById('btnStart').className = 'layui-btn layui-btn-sm layui-btn-normal';
        }
         //播放
        function playRecording() {
            recorder.play(audio);
            document.getElementById('btnStart').disabled = '';
            document.getElementById('btnStart').className = 'layui-btn layui-btn-sm layui-btn-normal';
            document.getElementById('btnStop').className = 'layui-btn layui-btn-sm layui-btn-disabled';
            document.getElementById('btnStop').disabled = "disabled";
        }
         //上传录音文件
        function uploadAudio() {
            recorder.upload("Handler1.ashx", function (state, e) {
                switch (state) {
                    case 'uploading':
                       
                        break;
                    case 'ok':
                      
                        alert("上传成功");
                        break;
                    case 'error':
                        alert("上传失败");
                        break;
                    case 'cancel':
                        alert("上传被取消");
                        break;
                }
            });
        }

    </script>

</body>
</html>
