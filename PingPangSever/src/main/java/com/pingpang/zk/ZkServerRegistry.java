package com.pingpang.zk;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pingpang.config.SpringContextUtil;
import com.pingpang.redis.RedisPre;
import com.pingpang.redis.service.RedisService;

public class ZkServerRegistry {

	//日志文件
	private Logger logger = LoggerFactory.getLogger(ZkServerRegistry.class);
	
	/**
	 * 1.server的根目录
	 * 2.子目录以接口的名字命名->值位IP:PORT
	 */
	private static final String PINGPANG_REGIST_SERVER="/PINGPANG_CHAT_SERVER";
	
	 /** session超时时间 */
    private static final int SESSION_OUTTIME = 10000;//ms
	
	private ZkClient zc;
	
	private static ZkServerRegistry zkSeverRegistry;

    private ZkServerRegistry() {};
    
    public static ZkServerRegistry getServerRegistry(String zkServer) {
    	if(null==zkSeverRegistry) {
    		zkSeverRegistry=new ZkServerRegistry();
    	}
    	
    	if(null==zkSeverRegistry.zc) {
				zkSeverRegistry.zc = new ZkClient(new ZkConnection(zkServer), ZkServerRegistry.SESSION_OUTTIME);
    	}
    	
    	return zkSeverRegistry;
    }
    
	/**
	  *   添加节点
	 * @param serviceSet 服务名称
	 * @param value 路径
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public boolean registAddServer(Set<String>serviceSet,String value){
        if(null==serviceSet || serviceSet.isEmpty() || null==value || "".equals(value.trim())) {
        	return false;
        }
        
        for(String str : serviceSet) {
        	this.crateNode(str, value);
        }
        
        return true;
	}
	
	/**
	 *    移除节点
	 * @param serviceSet 服务名称
	 * @param value 路径
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public boolean removeAddServer(Set<String>serviceSet,String value) throws KeeperException, InterruptedException {
		 if(null==serviceSet || serviceSet.isEmpty() || null==value || "".equals(value.trim())) {
	        	return false;
	        }
	        
	        for(String str : serviceSet) {
	        	this.removeNode(str, value);
	        }
	        
	        return true;
	}
	
	/**
	   *  创建节点
	 *    路径为  PINGPANG_REGIST_SERVER
	 *                            com.test
	 *                                    127.0.0.1
	 *                                    127.0.0.2  
	 * @param path
	 * @param value
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public boolean crateNode(String path,String value){
		if(null==path || "".equals(path) || null==value || "".equals(value.trim())) {
			return false;
		}
		
		String createPath=ZkServerRegistry.PINGPANG_REGIST_SERVER+"/"+path;
		
		if(!this.isExit(ZkServerRegistry.PINGPANG_REGIST_SERVER+"/"+path)) {
			this.zc.createPersistent(createPath, true);
		}
		
		createPath+="/"+value;
		
		if(!this.isExit(createPath)) {
			this.zc.createEphemeral(createPath);
		 }
		
		return true;
	}
	
	public boolean removeNode(String path,String value){
		if(null==path || "".equals(path) || null==value || "".equals(value.trim())) {
			return false;
		}
		
		String createPath=ZkServerRegistry.PINGPANG_REGIST_SERVER+"/"+path+"/"+value;
		
		if(this.isExit(createPath)) {
			return zc.delete(createPath);
		}
		
		return false;
	}
	
	/**
	  * 判断节点是否存在
	 * @param node
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public boolean isExit(String node){
		return this.zc.exists(node);
	}
	
	/**
	  * 获取接口下所有的服务器IP
	 * @param server
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public List<String> getServerPath(String server){
		List<String> serverList=new LinkedList<String>();
		if(null==server || "".equals(server.trim())) {
			return serverList ;
		}
		String path=ZkServerRegistry.PINGPANG_REGIST_SERVER+"/"+server;
		return zc.getChildren(path);
	}
	
	/**
	 * 关闭连接
	 * @throws InterruptedException 
	 */
	public void close(){
       if(null!=zc) {
    	   zc.close();
       }   		
	}
	
	/**
	  * 添加节点监控
	 */
	public void watchServer(String path) {
		/*
		zc.subscribeDataChanges(ZkServerRegistry.PINGPANG_REGIST_SERVER + "/" + path, new IZkDataListener() {

			@Override
			public void handleDataChange(String path, Object data) throws Exception {
				System.out.println("变更的节点为:" + path + ", 变更内容为:" + data);
			}

			@Override
			public void handleDataDeleted(String path) throws Exception {
				System.out.println("删除的节点为:" + path);
			}
		});
		*/
		
		zc.subscribeChildChanges(ZkServerRegistry.PINGPANG_REGIST_SERVER+"/"+path, new IZkChildListener() {
			@Override
			public void handleChildChange(String parentPath, List<String> currentChilds) throws Exception {
				logger.info("parentPath: " + parentPath);
				logger.info("currentChilds: " + currentChilds);
				
				RedisService redisService = SpringContextUtil.getBean(RedisService.class);
                
				Set<String> serveSet=redisService.getAllServer(RedisPre.SERVER_ADDRES_ZSET);
                
                if(null!=currentChilds && !currentChilds.isEmpty()) {
					Set<String> allServer=new HashSet<String>();
					allServer.addAll(currentChilds);
					allServer.addAll(serveSet);
					
					for(String str : allServer) {
						//不包含添加
						if(!serveSet.contains(str)) {
							redisService.addZset(RedisPre.SERVER_ADDRES_ZSET, str, 0);
						}
						
					    //不包含删除
					    if(!currentChilds.contains(str)) {
					    	redisService.delZset(RedisPre.SERVER_ADDRES_ZSET, str);
					    }
					}
				}else {
					//无数据暂且清楚服务
					logger.info("无数据,清除数据。。。");
					redisService.delete(RedisPre.SERVER_ADDRES_ZSET);
				}
			}
		});
	}
	
    public static void main(String[] args) throws InterruptedException{
    	ZkServerRegistry sr=ZkServerRegistry.getServerRegistry("127.0.0.1:2181");
    	sr.watchServer("com.pingpang.test");
    	
    	sr.removeNode("com.pingpang.test", "127.0.0.1:112"); 
    	sr.removeNode("com.pingpang.test", "127.0.0.1:113");
    	sr.removeNode("com.pingpang.test", "127.0.0.1:114");
    	sr.removeNode("com.pingpang.test", "127.0.0.1:115");
    	
    	sr.crateNode("com.pingpang.test", "127.0.0.1:112");
    	sr.crateNode("com.pingpang.test", "127.0.0.1:113");
    	sr.crateNode("com.pingpang.test", "127.0.0.1:114");
    	sr.crateNode("com.pingpang.test", "127.0.0.1:115");
    	
		
    	/*
		 * sr.removeNode("com.pingpang.test", "127.0.0.1:112");
		 * sr.removeNode("com.pingpang.test", "127.0.0.1:113");
		 * sr.removeNode("com.pingpang.test", "127.0.0.1:114");
		 * sr.removeNode("com.pingpang.test", "127.0.0.1:115");
		 * 
		 * sr.removeNode("com.pingpang.test", "");
		 */
		/*
		 * while(true) {
		 * 
		 * }
		 */
		
		List<String> server = sr.getServerPath("com.pingpang.test");
		for (String str : server) {
			System.out.println("===" + str);
		}
		
		for(;;) {
			
			Thread.sleep(600*1000);
		}
	}	
	
}
